require 'optparse'
require 'yaml'
require 'logger'

$logger = Logger.new(STDOUT)
$logger.level = Logger::WARN

class String
	def colorize(color_code)
		"\e[#{color_code}m#{self}\e[0m"
	end

	def red
		colorize(31)
	end

	def yellow
		colorize(33)
	end      

	def removeEmptyLines
		self.gsub(/^$\n/, '')
	end
end

class User
	attr_reader :path, :name, :actions
	def initialize(username)
		@name = username
		setPath
		setEmailClientName
		@procmailPath = "#{@path}/.procmailrc"
		@actions = []
		createProcmailrc
	end

	def getActions
        if @actions.size > 0
          print "#{@name}:\n"
          @actions.each do |action|
              puts "\t #{action}"
          end
        end
        @actions.size
	end

	def execRuleset(ruleset)
		ruleset.each do |rule|
			execRule(rule)
		end
	end

	private
	def setPath
		finger = `finger \"#{@name}\" 2>&1`.chomp
		if finger.include? "no such user."
			$logger.warn(finger.yellow)
			@status = :noSuchUser
			@path = nil
		elsif finger.include? "Directory: "
			@path = finger[/Directory: \K\/\S+/, 0]
			@status = :ok
		else
			$logger.error(finger.red)
			@status = :unknown
			@path = nil
		end
	end

	def setEmailClientName
		if @status != :ok
			@emailClientName = nil
			return
		elsif File.exists?(@path+"/mail/sent-mail") and File.exists?(@path+"/Mail/sent")
			@emailClientName = File.mtime(@path+"/mail/sent-mail") < File.mtime(@path+"/Mail/sent") ? :pine : :mutt
		elsif File.exists?(@path+"/mail/sent-mail")
			@emailClientName = :pine
		elsif File.exists?(@path+"/Mail/sent")
			@emailClientName = :mutt
		else
			@emailClientName = :unknown
		end
		if @emailClientName == :unknown
			$logger.warn("Cannot determine email client for #{@name}.".yellow)
		else
			$logger.info("#{@name} uses #{@emailClientName}.")
		end
	end

	def createProcmailrc
		if @status == :ok and not File.exist?(@procmailPath)
			File.open(@procmailPath, "a") 
			@actions << "Utworzono plik .procmailrc."
			$logger.info(".procmail created for #{@name} in #{@procmailPath}")
			changeOwnershipOfProcmailrc
		end
		rescue => error
			@status = :error
			$logger.error("Cannot create .procmailrc: #{error}".red)
			@actions << "Nie udało się utworzyć pliku .procmailrc".red
	end

	def changeOwnershipOfProcmailrc
		chown = `chown \"#{@name}\" \"#{@procmailPath}\" 2>&1`
		if $? == 0
			@actions << "Zmieniono właściciela pliku .procmailrc."
			$logger.info("chown #{@name} #{@procmail} - OK")
		else
			@actions << "Nie udało się zmienić właściciela pliku .procmailrc: #{chown}".red
			$logger.warn("Error in chown for #{@name}: #{chown}".yellow)
		end	
	end

	def execRule(rule)
		if @status == :ok and (@emailClientName == rule.emailClient.downcase.to_sym or rule.emailClient == "*")
			if rule.rule != nil and not checkIfRuleAlreadyExists(rule)
				appendRule(rule)
			end
		end
	end

	def checkIfRuleAlreadyExists(rule)
		procmailRules = File.read(@procmailPath)
		procmailRules.removeEmptyLines.include?(rule.rule.removeEmptyLines)
	rescue => error
		$logger.error("Cannot open #{@procmailPath} for #{@name}: #{error}".red)
		@actions << "Nie udało się otworzyć pliku .procmailrc #{@procmailPath} dla #{@name}.".red
		@status = :error
		return false
	end

	def appendRule(rule)
		File.write(@procmailPath, "\n#{rule.rule}", mode: 'a')
		$logger.info("Added rule #{rule.name} for #{@name}")
		@actions << "Dodano regułę #{rule.name}."
	rescue => error
		$logger.error("Cannot open #{@procmailPath} for #{@name}: #{error}".red)
		@actions << "Nie udało się otworzyć pliku .procmailrc #{@procmailPath} dla #{@name}.".red
		@status = :error
	end

end

class Rule
	attr_reader :name, :emailClient, :regex, :rule
	def initialize(ruleConfig)
		@name = ruleConfig['name'] || "unnamedRule"
		@emailClient = ruleConfig['emailClient']
		@regex = Regexp.new ruleConfig['regex'] if ruleConfig['regex']
		@rule = ruleConfig['rule']
		$logger.warn("#{@name} is empty.".yellow) unless @rule
	end

end

class Ruleset
	def initialize(filename)
		@rules = []
		YAML.load_file(filename).each do |i|
			@rules<<Rule.new(i)
		end
	end

	def method_missing(methodName, *args, &block)
		if @rules.respond_to?(methodName)
			@rules.send(methodName, *args, &block)
		else
			super.method_missing(methodName, *args, &block)
		end
	end
end

def summary(users)
    users.each do |user|
        user.getActions
    end
end

begin
users = []
OptionParser.new do |opts|
	opts.banner = "Usage: example.rb [options]"
	opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
			$logger.level = Logger::DEBUG
	end
	opts.on("-u", "--users", "Podaj listę użytkowników") do |u|
        if u
			ARGV.each do |user|
				users << User.new(user)
			end
	end
	end
	opts.on("-f", "--files", "Podaj listę plików") do |f|
		if f
			ARGV.each do |file|
				File.readlines(file).each do |line|
					users << User.new(line.chomp)
				end
			end
		end

	end
end.parse!

ruleset = Ruleset.new("rules.yaml")

if users.empty?
	$logger.warn("Nie wybrano żadnych użytkowników.".yellow)
	exit
end

users.each do |user|
    user.execRuleset(ruleset)
end

rescue => error
    $logger.unknown("Unexpected error: #{error}".red)
	raise error

rescue Interrupt => error
    $logger.error("Interrupted by user.".red) # {error}".red)

ensure
    summary(users)
end
